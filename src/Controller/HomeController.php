<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

class HomeController extends AbstractController
{

    /**
     * @Route("/path")
     */
    public function index(): Response
    {
       return $this->render('pages/home.html.twig', ['current_menu' => 'home']);
    }
}