<?php
namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;


class ProfilsController extends AbstractController
{
    private $repository;
    private $manager;

    public function __construct(UserRepository $repository, EntityManagerInterface $manager)
    {
        $this->repository = $repository;
        $this->manager = $manager;
    }

    /**
     * @Route("/path")
     */
    public function index(): Response
    {
        // $user = new User();
        // $user->setNom('Teboukeu')
        //     ->setPrenom('Fresnel')
        //     ->setEmail('teboukeufresnel@gmail.com')
        //     ->setProfils('Etudiant')
        //     ->setSexe('Homme');
        // $em = $this->getDoctrine()->getManager();
        // $em->persist($user);
        // $em->flush();
        $sexe = 'femme';
        $rep = $this->repository->getUserBySexe($sexe);
        // $rep[0]->setSexe('femme');
        $this->manager->flush();
        dump($rep);
       return $this->render('pages/profils.html.twig', ['current_menu' => 'profils','profils'=>$rep]);
    }
}